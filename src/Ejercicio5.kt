fun main(){//Escribe una función que calcule el total acumulado de una lista
    val arListaNumeros=introduceNumeros()

    println("La lista de numeros introducidos son: "+arListaNumeros)
    println("La suma de todos los ementos de la lista es: "+sumaLista(arListaNumeros))
}
//introduccion de los valores deseados en la lista
fun introduceNumeros():MutableList<Int>{
    val arLista= mutableListOf<Int>()
    var iCiclos:Int =0
    var sIntroduccion: Int=0

    println("Cuantos Numeros quiere en la lista: ")
    iCiclos=readLine()!!.toInt()

    for (i in 0..(iCiclos-1)){
        print("Introduce el elemento "+i+" de la lista: ")
        sIntroduccion= readLine()!!.toInt()
        arLista.add(sIntroduccion)
    }
    return arLista
}
//Calcula la suma de todos los ementos enteros de una lista
fun sumaLista(arConjuntoNumeros:MutableList<Int>):Int{
    var iSumatorio:Int= 0

    for (i in 0..(arConjuntoNumeros.size-1)){
        iSumatorio=iSumatorio+arConjuntoNumeros[i]
    }

    return iSumatorio
}