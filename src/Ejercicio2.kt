fun main(){//Función de escritura que invierte una lista, preferiblemente en su lugar
    var sLista= mutableListOf<String>()
    sLista=IntroduceLista()
    println("La lista introducida es: "+sLista)
    sLista=InvierteLista(sLista)
    println("La lista invertidfa es: "+sLista)

}
fun IntroduceLista():MutableList<String>{ //Introdducion de N valores para que esten dentro del array
    val sLista= mutableListOf<String>()
    var iCiclos:Int=0
    var sIntroduccion:String=" "

    println("¿Ciantos elementos quiere que tenga la lista?")
    iCiclos=readLine()!!.toInt()

    for (i in 1..iCiclos ){
        println("Introduzaca el elemento: "+i+" de la lista")
        sIntroduccion= readLine()!!
        sLista.add(sIntroduccion)
    }
    return sLista
}
//devuelve la lista invertida
fun InvierteLista(sLista:MutableList<String>):MutableList<String>{
    val iTamano:Int=sLista.size-1

    //Sustituye el valor que toma i en la posicion final del (array-i) para eveitar crear un segundo array

    var sIntermedia1:String=" "
    for (i in 0..(iTamano/2) ){
        sIntermedia1=sLista[i]
        sLista[i]=sLista[(iTamano-i)]
        sLista[(iTamano-i)]=sIntermedia1
    }
    return sLista
}