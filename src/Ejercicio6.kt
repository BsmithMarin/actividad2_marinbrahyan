fun main(){//Escribe una función que pruebe si una cadena es un palíndromo.
    println("Ejemplos de palindromos:")
    println("'atar a la rata'")
    println("'Amor azul Ramera de todo te di Mariposa colosal si yo de todo te di Poda la rosa Venus El atomo como tal es un evasor alado Pide todo te doy isla sol ocaso piramide Todo te dare mar luz aroma'")
    println("'no deseo yo ese don'")
    println("12344321")
    println("Introduzca un palabra o texto para evaluar si es palindromo o no (No incluya tildes ni caracteres esciales): ")

    val strPalabra:String= readLine()!!.toUpperCase()

    if(esPalindromo(DePalabraAArray(strPalabra))==true){
        println("El texto indroducido SI ES un palindromo")
    }else{
        println("El texto introducido NO!! es un palindromo")
    }
}
//Mete los caracteres de una palabra en una lista
fun DePalabraAArray(strPalabra:String):MutableList<String>{
    val arLista= mutableListOf<String>()
    var sValorI:String=" "
    /*
    No se incluyen los ESPACIOS en el array que guarda los caracteres del texto
    para que pueda funcionar con textos de mas de una palabra, tal y como se ve en los ejemplos
    impresos en consola al principio del programa
     */
    for (i in strPalabra){
        sValorI=i.toString()
        if(sValorI!=(" ")) {
            arLista.add(sValorI)
        }
    }
    return arLista
}
//Devuelve en un Booleano si la cadena de caracteres introducida es palindromo o no
fun esPalindromo(arLista:MutableList<String>):Boolean{
    var boEsPalindromo:Boolean=true
    var iCiclos:Int=(arLista.size-1)/2
    val iUltimoIndice:Int=(arLista.size-1)
    /*
    El numero de vueltas es la mitad (Division entera) para comprobar que los caracteres coinciden desde un punto central
    es decir si son simetricos desde un elemento central, en las palabras con elementos impares, este elemento central no
    se evalua en ningún momento, pues la letra central es donde se situal el "Espejo"
     */
    for (i in 0..iCiclos){
        if(arLista[i]!=arLista[(iUltimoIndice-i)]){
            boEsPalindromo=false
        }
    }
    return boEsPalindromo
}