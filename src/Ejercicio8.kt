/*
Escribe una función que tome una lista de cadenas y las imprima, una por línea, en un marco rectangular. Por ejemplo, la lista ["Hola", "Mundo", "en", "un", "marco"]:
*/

fun main(){
    println("Las palabras introducidas en la lista apareceran en un recuadro rectangular")
    enmarcaListas(introduceCadenas())
}

fun enmarcaListas (arsListaEnmarcar:MutableList<String>){

// Enmarca en un rectangulo perfecto compuesto de asteriscos, cadenas de caracteres de hasta 31 caracteres
// contando los espacios, en caso de que la cadena sea mas larga, se sale del rectangulo e imprime un '*' a continuacion

    var iContador:Int=0
    println("***********************************")
    for (i in arsListaEnmarcar){
        print("*  "+i)
        for (j in i){
            iContador=iContador+1
        }
        for (k in 1..(31-iContador)){
            print(" ")
        }
        print("*")
        iContador=0
        println()
    }
    println("***********************************")
}

//Introduccion de los elementos que formaran parte de la lista, asi como su numero
fun introduceCadenas():MutableList<String>{
    val arsLista= mutableListOf<String>()
    println("Cuantos elemenos quiere en la lista: ")
    var iCiclos:Int =readLine()!!.toInt()
    var sIntroduccion: String=" "
    for (i in 0..(iCiclos-1)){
        print("Introduce el elemento "+i+" de la lista: ")
        sIntroduccion= readLine()!!.toUpperCase()
        arsLista.add(sIntroduccion)
    }
    return arsLista
}