fun main(){//Escribe una función que verifique si un elemento aparece en una lista.
    val arLista: MutableList<String> =introduceLista()
    var sComprobacion: String= " "
    var bSeEncuentra: Boolean=false
    var iPosicion:Int=0
    var iContador:Int=0

    println("Introduzca el elemento que quiere comprobar si se encuentra en la lista: ")

    sComprobacion=readLine()!!.toUpperCase()

    for (i in arLista){
        if(i==sComprobacion){       //Comprueba en cada iteracion si el elemento coincide con el buscado
            bSeEncuentra=true       //Ademas guarda en que posicion de la lista se encuentra, contando desde '0'
            iPosicion=iContador
        }
        iContador=iContador+1
    }
    if(bSeEncuentra==true){
        println("el elemento se ecnuentra en la lista, en la posicion "+iPosicion)
    }else{
        println("El elemento no se encuentra en la lista")
    }
}

//introduccion de los valores deseados en la lista
fun introduceLista():MutableList<String>{
    println("Cuantos elemenos quiere en la lista: ")

    val arLista= mutableListOf<String>()
    var iCiclos:Int =readLine()!!.toInt()
    var sIntroduccion: String=" "

    for (i in 0..(iCiclos-1)){
        print("Introduce el elemento "+i+" de la lista: ")
        sIntroduccion= readLine()!!.toUpperCase()
        arLista.add(sIntroduccion)
    }
    return arLista
}