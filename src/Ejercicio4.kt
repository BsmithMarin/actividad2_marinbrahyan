fun main(){ //Escribe una función que devuelva los elementos en posiciones impares en una lista.
    val arLista=introduceArray()

    println("La lista introducida es: "+arLista)
    print("Los elementos en posiciones impares de la lista son: "+devuelveImpares(arLista))
}

//introduccion de los valores deseados en la lista
fun introduceArray():MutableList<String>{
    val arLista= mutableListOf<String>()
    var iCiclos:Int =0
    var sIntroduccion: String=" "

    println("Cuantos elemenos quiere en la lista: ")

    iCiclos=readLine()!!.toInt()

    for (i in 0..(iCiclos-1)){
        print("Introduce el elemento "+i+" de la lista: ")
        sIntroduccion= readLine()!!.toUpperCase()
        arLista.add(sIntroduccion)
    }
    return arLista
}

//devuelve las posiciones impares de una lista, reemplazando la propia lista, sin recurrir a una segunda lista.
fun devuelveImpares(arLista:MutableList<String>):MutableList<String>{

    val iTamano:Int=arLista.size-1
    var iContador:Int=0

    for(i in 0..iTamano){
        /*
        Consideramos que el primer elemento introducido por consola es el que tiene la posicion 1, es decir arLista[0] es el primero;
        por lo que las posiciones pares o con modulo 0 de 2, son las impares y por tanto, las que no se eliminan de la lista.
         */
        if(i%2!==0){
            arLista.remove(arLista[(i-iContador)])
            iContador=iContador+1
        }
    }
    return arLista
}