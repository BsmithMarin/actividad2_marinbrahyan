class ProductoVending {
    var sNombre:String=""
    var iStock:Int=0
    var dPrecio:Double=0.00

    constructor(nombre:String,stock:Int,precio:Double){
        sNombre=nombre
        iStock=stock
        dPrecio=precio
    }

    fun Reponer(iUnidades:Int){
        iStock=iStock+iUnidades
    }


}