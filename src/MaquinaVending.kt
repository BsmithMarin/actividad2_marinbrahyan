import kotlin.math.round


class MaquinaVending {
    var strModelo:String="SV-25200"
    var strCiudad:String=""
    var strDireccion:String=""
    var iCodigoPostal:Int=0
    var arrProductos= arrayListOf<ProductoVending>()

    constructor(Ciudad:String,Direccion:String,CodigoPostal:Int){
        strCiudad=Ciudad
        strDireccion=Direccion
        iCodigoPostal=CodigoPostal
    }
    fun AgregaProducto(Producto:ProductoVending){
        arrProductos.add(Producto)
    }

    fun Venta (CodigoProducto:Int){     //Venta de productos de la maquina

        if((arrProductos[(CodigoProducto)].iStock)>0){

            var dEleccionOImporte:Double=0.0
            val sNombre:String=arrProductos[CodigoProducto].sNombre
            val dPrecio:Double=arrProductos[CodigoProducto].dPrecio
            var dImporteTotal:Double=0.00                      //Se almacena en otras varibales, las variables del array
            var dDiferencia:Double=0.0                         //Productos para que sea mas legible el código

            println("EL precio del producto: "+sNombre+" es: "+dPrecio+"€")
            print("Introduzca el importe o pulse 0 para salir: ")
            println("")
            dEleccionOImporte= readLine()!!.toDouble()
            if(dEleccionOImporte>0.00){
                dImporteTotal=dImporteTotal+dEleccionOImporte

                //Mientras no introduzca el dinero total del producto se pide mas y se indica
                //cuanto falta, siempre puede pulsar '0' para cancelar y recuperar dinero introducido

                while ((dImporteTotal<dPrecio)&&(dEleccionOImporte!=0.00)){
                    dDiferencia=(dPrecio-dImporteTotal)
                    println("Ha introducido "+dImporteTotal+"€"+" le faltan "+ (round(dDiferencia*100))/100 +"€"+
                            "  Pulse 0 si desea cancelar y recuperar el dinero")
                    println("Introduza importe: ")
                    dEleccionOImporte= readLine()!!.toDouble()
                    dImporteTotal=dImporteTotal+dEleccionOImporte
                }

                            //Devulve el cambio y resta una unidad de producto en la maquina

                if(dImporteTotal>=dPrecio){
                    println("Aqui tiene su producto, el cambio son "+(round((dImporteTotal-dPrecio)*100))/100 +"€")
                    arrProductos[CodigoProducto].iStock = arrProductos[CodigoProducto].iStock-1
                    println("Qquedan tantas unidades de producto: "+arrProductos[CodigoProducto].iStock)
                }else {
                             //Se le devuelve el dinero al usuario en caso de que cancele la compra

                    println("Operacion cancelada, le seran devultos: " + dImporteTotal + "€")
                }
            }

        }else{                          //Si no hay suficiente producto

            println("Producto no disponible")
        }
    }
}