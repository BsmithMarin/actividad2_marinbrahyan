fun main(){//Escribe una función que devuelva el elemento más grande de una lista.
    println("Introduzca valores enteros para que formen parte de la lista, y despues devolverle el mayor de todos los numeros introducidos")
    println("Para finalizar la introduccion de valores introduzca '-501' ")
    var iNumeros= arrayListOf<Int>()
    var iContador:Int=1
    var iInput:Int=0
    var iMayor:Int =0

    do{
        println("Introduzca el elemento "+(iContador)+" de la lista: " )
        iInput= readLine()!!.toInt()
        if(iInput!=-501){
            iNumeros.add(iInput)
        }
        iContador=iContador+1
    }while(iInput!=-501);

    println("la lista es: "+iNumeros)

    for(i in iNumeros){
        if (i>iMayor){
            iMayor=i;
        }
    }

    println("El mayor de los elementos de la lista es: "+iMayor)
}