
/*
Escriba un programa que convierta automáticamente texto en inglés a código Morse y
viceversa.
     */
fun main(){
        var sTextoATraducir:String?=" "
        var arraysTRaduccion= mutableListOf<String?>()
        var iEleccion:Int=0

        println("¿Para traducir del morse al ingles pulse 1, para hacerlo del ingles al morse pulse 2")
        iEleccion=readLine()!!.toInt()

        while(iEleccion>2 || iEleccion<0){  //Obliga al usuario a introducir una opcion valida
            println("Elija la opcion 1 o la 2, no se admiten valores distintos")
            iEleccion=readLine()!!.toInt()
        }

        if(iEleccion==1){
            println("Introduzca el texto en morse a traducir, pulsando intro despues de cada letra e introducciendo '/' para lograr un espacio")
            println("Escriba EXIT para terminar y ver la traduccion de morse a ingles, si se equivoca aprecera null en la traduccion")
            do{
                //Añade al array Traduccion las devoluciones que da la funcion de Morse a Ingles

                sTextoATraducir=readLine()!!.toUpperCase()
                arraysTRaduccion.add(DeMorseAIngles(sTextoATraducir))
            }while(sTextoATraducir!="EXIT");

            println("La traduccion en ingles= "+arraysTRaduccion)

        }else{

            //Pasa directamente el texto a traducir y devulve un arraay con la traduccion

            println("Introduzca el texto en ingles a traducir")
            sTextoATraducir=(readLine()!!.toUpperCase()).toString()
            println("La traduccion en Morse es: "+DeInglesAMorse(sTextoATraducir))
        }

}
//Mapa para traducir del morse al inglés
fun DeMorseAIngles(Texto:String?):String?{
    var sTraduccion: String?=" "
    val morseAIngles: Map<String?,String?> = mapOf(".-" to "A","-..." to "B", "-.-." to "C", "----" to "CH",
            "-.." to "D","." to "E","..-." to "F","--." to "G","...." to "H",".." to "I",".---" to "J","-.-" to "K",
            ".-.." to "L","--" to "M","-." to "N","---" to "O",".--." to "P","--.-" to "Q",".-." to "R","..." to "S",
            "-" to "T","..-" to "U","...-" to "V",".--" to "W","-..-" to "X","-.--" to "Y","--.." to "Z","-----" to "0",
            ".----" to "1","..---" to "2","...--" to "3","....-" to "4","....." to "5","-...." to "6","--..." to "7",
            "---.." to "8","----." to "9","/" to " ","EXIT" to "END")
    sTraduccion=morseAIngles[Texto]         //Mete la traduccion del caracter en el array traduccion
    return sTraduccion
}
//Mapa para traduccir del ingles al morse
fun DeInglesAMorse(Texto: String):MutableList<String?>{
    val arrsTraduccion= mutableListOf<String?>()
    val morseAIngles: Map<String?,String?> = mapOf("A" to ".-","B" to "-...", "C" to "-.-.", "CH" to "----",
            "D" to "-..","E" to ".","F" to "..-.","G" to "--.","H" to "....","I" to "..","J" to ".---","K" to "-.-",
            "L" to ".-..","M" to "--","N" to "-.","O" to "---","P" to ".--.","Q" to "--.-","R" to ".-.","S" to "...",
            "T" to "-","U" to "..-","V" to "...-","W" to ".--","X" to "-..-","Y" to "-.--","Z" to "--..","0" to "-----",
            "1" to ".----","2" to "..---","3" to "...--","4" to "....-","5" to ".....","6" to "-....","7" to "--...",
            "8" to "---..","9" to "----."," " to "/","EXIT()" to "END")
    var sCaracter:String=" "

    //Busca en el diccionario la traduccion de cada caracter ingles y lo mete en un array traduccion que se devulve al
    // final de la funcion

    for (i in Texto){
        sCaracter=i.toString()
        arrsTraduccion.add(morseAIngles[sCaracter])
    }
    return arrsTraduccion
}