
fun main(){
    var iEleccion:Int=0

    val cocacola1:ProductoVending=ProductoVending("Cocacola",15,1.00)
    val Fanta1:ProductoVending=ProductoVending("Fanta",15,1.20)
    val Nestea1:ProductoVending=ProductoVending("Nestea",15,1.30)     //Crea todos los productos
    val Sprite1:ProductoVending=ProductoVending("Sprite",15,1.40)
    val Doritos1:ProductoVending=ProductoVending("Doritos",15,1.50)
    val TabletaChocolate1:ProductoVending=ProductoVending("Tableta de Chocolate",15,1.60)
    val TortaArroz1:ProductoVending=ProductoVending("Torta De Arroz",15,1.70)
    val Cafe1:ProductoVending=ProductoVending("Cafe",15,1.80)
    val Caramelos1:ProductoVending=ProductoVending("Caramelos",15,1.90)
    val Sandwich1:ProductoVending=ProductoVending("Sandwich",15,2.00)

//Crea la maquina
    var Maquina1:MaquinaVending=MaquinaVending("Madrid","C/ Claveles, 23, Vestibulo planta baja",28512)

    Maquina1.AgregaProducto(cocacola1)
    Maquina1.AgregaProducto(Fanta1)
    Maquina1.AgregaProducto(Nestea1)
    Maquina1.AgregaProducto(Sprite1)           //Añade los productos a la maquina
    Maquina1.AgregaProducto(Doritos1)
    Maquina1.AgregaProducto(TabletaChocolate1)
    Maquina1.AgregaProducto(TortaArroz1)
    Maquina1.AgregaProducto(Cafe1)
    Maquina1.AgregaProducto(Caramelos1)
    Maquina1.AgregaProducto(Sandwich1)

    while(1<20){                                //Ejecuta permanentemente la maquina de Vending

        EnmarcaMenu(Maquina1.arrProductos)      //Menú inicial, se muestran los nombres de los productos
                                                //Y sus respectivos códigos
        do {
            print("Seleccione opcion: ")
            iEleccion = readLine()!!.toInt()    //Obliga a que se elija un codigo entre 1 y 10
        }while(iEleccion<1 || iEleccion>10);

        Maquina1.Venta((iEleccion-1))
    }
}

fun EnmarcaMenu (arsListaEnmarcar:ArrayList<ProductoVending>){
/*
Crea un menu para la presentacion de los productos, utilizando uan funcion similar a la del
ejercicio8.kt
 */

    var iContador:Int=0
    var iCiclos:Int=0

    for (z in 0..210) {
        print("*")
    }
    println("")
    println("BIENVENIDO A SUPERVENDING")
    for (z in 0..210) {
        print("*")
    }

    println("")
    //Inprime el nombre de los productos y su opcion, asignandoles 70 caracteres de espacio a cada uno
    //dentro del recuadro, imprime 3 productos por linea
    for(i in arsListaEnmarcar){
        if(iCiclos%3==0 && iCiclos!=0){
            println("*")
        }
        print("* Pulse "+(iCiclos+1)+" para seleccionar "+i.sNombre)
        for(k in i.sNombre){
            iContador=iContador+1
        }
        for(j in 0..(70-iContador-28)){
            print(" ")
        }
        iContador=0
        iCiclos=iCiclos+1
    }

    println("")

    for (y in 0..210){
        print("*")
    }
    println("")
}
